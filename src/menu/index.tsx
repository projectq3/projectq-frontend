import { ReactElement } from "react";
import { PieChartOutlined } from "@ant-design/icons";
import { FaUserDoctor, FaBed, FaUserPlus } from "react-icons/fa6";

const getItem = (
  label: string,
  key: string,
  icon: ReactElement,
  children?: any[]
) => {
  return {
    key,
    icon,
    children,
    label,
  };
};

const items = [
  getItem("TRANG CHỦ", "", <PieChartOutlined />),
  getItem("THỂ LOẠI", "Category", <FaUserPlus />, [
    getItem("THỂ LOẠI1", "Category1", <FaUserDoctor />),
    getItem("THỂ LOẠI2", "Category2", <FaBed />),
  ]),
  getItem("GAME ĐIỆN THOẠI", "phoneGame", <FaUserPlus />, [
    getItem("GAME ĐIỆN THOẠI1", "phoneGame1", <FaUserDoctor />),
    getItem("GAME ĐIỆN THOẠI2", "phoneGame2", <FaBed />),
  ]),
];

export default items;
