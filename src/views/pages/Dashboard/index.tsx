import {
  CommentOutlined,
  CustomerServiceOutlined,
  InfoCircleOutlined,
  LikeOutlined,
  MessageOutlined,
  PlayCircleOutlined,
  StarOutlined,
} from "@ant-design/icons";
import {
  Avatar,
  Button,
  Card,
  Carousel,
  Col,
  FloatButton,
  List,
  Popconfirm,
  Row,
  Skeleton,
  Space,
} from "antd";
import Meta from "antd/lib/card/Meta";
import {
  CardMyProfile,
  CarouselComponent,
  DropdownHeader,
} from "src/components";
import { images } from "src/assets";
import {
  Autoplay,
  FreeMode,
  Navigation,
  Pagination,
  Parallax,
  Thumbs,
} from "swiper/modules";
import { Swiper, SwiperSlide } from "swiper/react";
import React, { useEffect, useRef, useState } from "react";
import { PaginationProps } from "antd/lib";

export default function Dashboard() {
  const [loading, setLoading] = useState(false);
  const progressCircle = useRef(null);
  const progressContent = useRef(null);
  const onAutoplayTimeLeft = (s, time, progress) => {
    progressCircle.current.style.setProperty("--progress", 1 - progress);
    progressContent.current.textContent = `${Math.ceil(time / 5000)}s`;
  };

  const slidesData = [
    {
      imageUrl: images.carousel.carousel1,
      rating: 8.5,
      title: "The Queen's Gambit The Queen's GambitThe Queen's GambitThe Queen's Gambit",
    },
    {
      imageUrl: images.carousel.carousel2,
      rating: 8.5,
      title: "The Queen's Gambit",
    },
    {
      imageUrl: images.carousel.carousel3,
      rating: 8.5,
      title: "The Queen's Gambit",
    },
    {
      imageUrl: images.carousel.carousel4,
      rating: 8.5,
      title: "The Queen's Gambit",
    },
    {
      imageUrl: images.carousel.carousel5,
      rating: 8.5,
      title: "The Queen's Gambit",
    },
    {
      imageUrl: images.carousel.carousel6,
      rating: 8.5,
      title: "The Queen's Gambit",
    },
    {
      imageUrl: images.carousel.carousel7,
      rating: 8.5,
      title: "The Queen's Gambit",
    },
    {
      imageUrl: images.carousel.carousel7,
      rating: 8.5,
      title: "The Queen's Gambit",
    },
    {
      imageUrl: images.carousel.carousel7,
      rating: 8.5,
      title: "The Queen's Gambit",
    },
    {
      imageUrl: images.carousel.carousel7,
      rating: 8.5,
      title: "The Queen's Gambit",
    },
    {
      imageUrl: images.carousel.carousel7,
      rating: 8.5,
      title: "The Queen's Gambit",
    },
    {
      imageUrl: images.carousel.carousel7,
      rating: 8.5,
      title: "The Queen's Gambit",
    },
    {
      imageUrl: images.carousel.carousel7,
      rating: 8.5,
      title: "The Queen's Gambit",
    },
  ];

  const Item = ({ imgSrc, title, description, rating, episodeStatus }) => (
    <div className="item">
      <p className="aspect-ratio-box aspect-ratio-box--16-9">
        <img
          className="aspect-ratio-box__in"
          src={imgSrc}
          width="320"
          height="180"
          alt="Image…"
          style={{ borderRadius: 10 }}
        />
      </p>
      <div className="title">
        <h3>{title}</h3>
      </div>
      {/* <div className="des">
        <span>{description}</span>
      </div> */}
      <div className="rating">
        <span>{rating}</span>
      </div>
      <div className="episode-status">
        <span>{episodeStatus}</span>
      </div>
      <div className="icon-play">
        <PlayCircleOutlined />
      </div>
    </div>
  );

  return (
    <div>
      <div className="layout">
        <div className="home">
          <section>
            <div className="content swiper-home">
              <>
                <Swiper
                  spaceBetween={30}
                  centeredSlides={true}
                  autoplay={{
                    delay: 5000,
                    disableOnInteraction: false,
                  }}
                  pagination={{
                    clickable: true,
                  }}
                  navigation={true}
                  modules={[Autoplay, Pagination, Navigation]}
                  onAutoplayTimeLeft={onAutoplayTimeLeft}
                  className="mySwiper"
                >
                  {slidesData.map((slide, index) => (
                    <SwiperSlide key={index}>
                      <div
                        className="swiper-slide swiper-slide-home"
                        style={{
                          backgroundImage: `linear-gradient(to top, #0f2027, #203a4300, #2c536400), url("${slide.imageUrl}")`,
                          backgroundPosition: "center",
                          backgroundSize: "cover",
                        }}
                      >
                        <span className="rating">{slide.rating}</span>
                        <div className="title-container">
                          <h2>{slide.title}</h2>
                        </div>
                      </div>
                    </SwiperSlide>
                  ))}
                  <div className="autoplay-progress" slot="container-end">
                    <svg viewBox="0 0 48 48" ref={progressCircle}>
                      <circle cx="24" cy="24" r="20"></circle>
                    </svg>
                    <span ref={progressContent}></span>
                  </div>
                </Swiper>
              </>
            </div>
            <ul className="circles">
              <li></li>
              <li></li>
              <li></li>
              <li></li>
              <li></li>
              <li></li>
              <li></li>
              <li></li>
              <li></li>
              <li></li>
            </ul>
          </section>
          <div className="carousel">
            <section>
              <div className="content">
                <Swiper
                  slidesPerView={8}
                  spaceBetween={30}
                  pagination={{
                    clickable: true,
                  }}
                  modules={[Pagination]}
                  className="mySwiper"
                >
                  {slidesData.map((slide, index) => (
                    <SwiperSlide key={index}>
                      <div
                        className="swiper-slide"
                        style={{
                          background: `linear-gradient(to top, #0f2027, #203a4300, #2c536400), url("${slide.imageUrl}") no-repeat 50% 50% / cover`,
                        }}
                      >
                        <span>{slide.rating}</span>
                        <div>
                          <h2>{slide.title}</h2>
                        </div>
                        <div
                          className="icon-play-swiper"
                          style={{ transition: "visibility 0.5s" }}
                        >
                          <PlayCircleOutlined />
                        </div>
                      </div>
                    </SwiperSlide>
                  ))}
                </Swiper>
              </div>
            </section>
          </div>
          <section>
            <div className="content">
              <div className="product">
                <div className="container">
                  {slidesData.map((slide, index) => (
                    <Item
                      imgSrc={slide.imageUrl}
                      title={slide.title}
                      description={slide.imageUrl}
                      rating={slide.rating}
                      episodeStatus={slide.rating}
                    />
                  ))}
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  );
}
