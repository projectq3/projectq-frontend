// Share
export { default as AddButton } from "./shared/Button/AddButton";
export { default as DeleteButton } from "./shared/Button/DeleteButton";
export { default as EditButton } from "./shared/Button/EditButton";
export { default as OnDeleteButton } from "./shared/Button/OnDeleteButton";
export { default as SuccesButton } from "./shared/Button/SuccesButton";
export { default as CardSave } from "./shared/Card/CardSave";
export { default as SearchInput } from "./shared/Input/SearchInput";
export { default as DeleteConfirmModal } from "./shared/Modal/DeleteConfirmModal";
export { default as AvatarName } from "./shared/Table/AvatarName";
export { default as CardProfile } from "./shared/Card/CardProfile";
export { default as IconDateCreate } from "./shared/List/IconDateCreate";
export { default as IconDocument } from "./shared/List/IconDocument";
export { default as DropdownHeader } from "./shared/Dropdown/DropdownHeader";
export { default as BreadcrumbComponent } from "./shared/Breadcrumb/BreadcrumbComponent";
export { default as CardDisease } from "./shared/Card/CardDisease";
export { default as CardImageListNoDelete } from "./shared/Card/CardImageListNoDelete";
export { default as CardMyProfile } from "./shared/Card/CardMyProfile";
export { default as CarouselComponent } from "./shared/Carousel/CarouselComponent";

// Icon
export { default as IconEdit } from "./shared/Icon/IconEdit";
export { default as IconDelete } from "./shared/Icon/IconDelete";
export { default as IconTest } from "./shared/Icon/IconTest";
export { default as IconViewDetail } from "./shared/Icon/IconViewDetail";
export { default as IconCheckNow } from "./shared/Icon/IconCheckNow";
export { default as IconIntroduce } from "./shared/Icon/IconIntroduce";
export { default as IconAddAmount } from "./shared/Icon/IconAddAmount";
export { default as IconAdd } from "./shared/Icon/IconAdd";
export { default as IconCancel } from "./shared/Icon/IconCancel";
export { default as IconSetting } from "./shared/Icon/IconSetting";
export { default as IconConfirm } from "./shared/Icon/IconConfirm";
export { default as IconPhone } from "./shared/Icon/IconPhone";
export { default as IconCheck } from "./shared/Icon/IconCheck";
export { default as IconLocation } from "./shared/Icon/IconLocation";

// OrderTests
export { default as CardTypeTest } from "./OrderTests/CardTypeTest";
export { default as PersonalInfoFields } from "./OrderTests/PersonalInfoFields";
export { default as SelectedTestType } from "./OrderTests/SelectedTestType";
export { default as TestPackage } from "./OrderTests/TestPackage";
export { default as TestTypeButtons } from "./OrderTests/TestTypeButtons";
export { default as UploadSection } from "./OrderTests/UploadSection";

// Medicine
export { default as AddQuantityMedicineModal } from "./Medicine/AddQuantityMedicineModal";
export { default as CreateMedicineModal } from "./Medicine/CreateMedicineModal";
export { default as ImportExcelMecineModal } from "./Medicine/ImportExcelMecineModal";
export { default as UpdateMedicineModal } from "./Medicine/UpdateMedicineModal";

//Service
export { default as CreateServiceModal } from "./Service/CreateServiceModal";
export { default as EditServiceModal } from "./Service/EditServiceModal";
// Dr
export { default as ModalDelete } from "./Dr/ModalDelete";
export { default as ModalUpdate } from "./Dr/ModalUpdate";

// span {}
export { default as SpanMailTo } from "./shared/span/SpanMailTo";
export { default as SpanPhone } from "./shared/span/SpanPhone";
