import { Form, Input, Radio, Select } from "antd";

function PersonalInfoFields({
  isImageConfirmationNeeded,
  handleChangeProvince,
  handleChangeistrict,
  listProvinces,
  listDistrict,
  listWard,
}) {
  return (
    <div className="order-content">
      <Form.Item
        // label="Họ và tên"
        name="member_name"
        rules={[
          {
            required: true,
            message: "Họ & Tên không được để trống",
            min: 2,
            max: 191,
          },
        ]}
      >
        <div className="form-center-30">
          <label>Họ và tên :</label>
          <Input />
        </div>
      </Form.Item>
      {isImageConfirmationNeeded ? (
        ""
      ) : (
        <Form.Item
          // label="Ngày sinh"
          name="member_dob"
          rules={[
            {
              required: true,
              message: "Ngày sinh không được để trống",
            },
          ]}
        >
          <div className="form-center-30">
            <label>Ngày sinh :</label>
            <Input type="date" />
          </div>
        </Form.Item>
      )}
      {isImageConfirmationNeeded ? (
        ""
      ) : (
        <Form.Item
          // label="Giới tính"
          name="member_gender"
          rules={[
            {
              required: true,
              message: "Giới tính không được để trống",
            },
          ]}
        >
          <div className="form-center-30">
            <label>Giới tính :</label>
            <Radio.Group>
              <Radio value="male">Nam</Radio>
              <Radio value="female">Nữ</Radio>
            </Radio.Group>
          </div>
        </Form.Item>
      )}
      {isImageConfirmationNeeded ? (
        ""
      ) : (
        <Form.Item
          // label="Gmail"
          name="member_email"
          rules={[
            {
              required: true,
              message: "Email không được để trống",
            },
          ]}
        >
          <div className="form-center-30">
            <label>Gmail :</label>
            <Input type="email" />
          </div>
        </Form.Item>
      )}

      <Form.Item
        // label="Số điện thoại"
        name="member_phone"
        rules={[
          {
            required: true,
            message: "Số điện thoại không được để trống",
            max: 11,
            min: 9,
          },
        ]}
      >
        <div className="form-center-30">
          <label>Số điện thoại :</label>
          <Input />
        </div>
      </Form.Item>
      <Form.Item
        // label="Danh sách tỉnh thành"
        name="province_id"
        rules={[
          {
            required: true,
            message: "Địa chỉ không được để trống",
          },
        ]}
      >
        <div className="form-center-30">
          <label>Danh sách tỉnh thành :</label>
          <Select
            style={{ width: "100%" }}
            onChange={(e) => {
              handleChangeProvince(e);
            }}
            options={listProvinces.map((item) => ({
              value: item.matp,
              label: item.name,
            }))}
          />
        </div>
      </Form.Item>
      <Form.Item
        // label="Danh sách quận huyện"
        name="district_id"
        rules={[
          {
            required: true,
            message: "Địa chỉ không được để trống",
          },
        ]}
      >
        <div className="form-center-30">
          <label>Danh sách quận huyện :</label>
          <Select
            style={{ width: "100%" }}
            // value={secondCity}
            onChange={(e) => {
              handleChangeistrict(e);
            }}
            options={listDistrict.map((item) => ({
              value: item.maqh,
              label: item.name,
            }))}
          />
        </div>
      </Form.Item>
      <Form.Item
        // label="Danh sách thị xã"
        name="ward_id"
        rules={[
          {
            required: true,
            message: "Địa chỉ không được để trống",
          },
        ]}
      >
        <div className="form-center-30">
          <label>Danh sách thị xã :</label>
          <Select
            style={{ width: "100%" }}
            options={listWard.map((item) => ({
              value: item.xaid ? item.xaid : null,
              label: item.name ? item.name : null,
            }))}
          />
        </div>
      </Form.Item>

      <Form.Item
        // label="Địa chỉ cụ thể"
        name="short_address"
        rules={[
          {
            required: true,
            message: "Địa chỉ không được để trống",
          },
        ]}
      >
        <div className="form-center-30">
          <label>Địa chỉ cụ thể :</label>
          <Input type="text" />
        </div>
      </Form.Item>
    </div>
  );
}

export default PersonalInfoFields;
