import { Form, Radio } from 'antd';

function TestPackage({ name, label, rules, data }){
    return (
        <Form.Item name={name} rules={rules} label={label}>
          <Radio.Group buttonStyle="solid">
            {data.map((item) => (
              <Radio.Button
                key={item.id}
                value={item.id}
                style={{ marginBottom: "1rem", width: "100%" }}
              >
                {item.name} - {item.price_formated}
              </Radio.Button>
            ))}
          </Radio.Group>
        </Form.Item>
      );
};

export default TestPackage;