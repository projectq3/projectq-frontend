import { Card } from "antd";
import React from "react";

function SelectedTestType({ title , categories, selectedValues, isImageConfirmationNeeded }) {
  if (isImageConfirmationNeeded) {
    return null;
  }
  return (
    <Card style={{ height: "unset", marginBottom: "1rem" }}>
      <div
        style={{
          fontWeight: "bold",
          marginBottom: "1rem",
        }}
      >
        {title}
      </div>
      <div>
        <ul>
          {categories.map((cat) =>
            cat.type_tests.map((test) => {
              if (selectedValues.includes(test.id)) {
                const formattedPrice = test.price.toLocaleString(undefined, {
                  style: "currency",
                  currency: "VND", // Replace with the desired currency code
                });
                return (
                  <li key={test.id}>
                    {test.name} - {formattedPrice}
                  </li>
                );
              }
              return null;
            })
          )}
        </ul>
      </div>
    </Card>
  );
}

export default SelectedTestType;
