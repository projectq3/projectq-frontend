import React from "react";

function AvatarName({ avatar, name }) {
  return (
    <div className="item-avatar-profile">
      <div className="avatar-container">
        {avatar && (
          <img
            className="avatar"
            src={avatar}
            alt="Avatar"
            style={{ cursor: "pointer" }}
          />
        )}
        <div className="item-text-profile">
          <span>{name}</span>
        </div>
      </div>
    </div>
  );
}

export default AvatarName;
