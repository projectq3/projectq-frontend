import React from "react";
import { FaCircleCheck } from "react-icons/fa6";

const iconActive = ({ Click }) => {
  return <FaCircleCheck onClick={Click} />;
};

export default iconActive;
