import React from "react";
import { FaLocationDot } from "react-icons/fa6";

function IconLocation(props) {
  return <FaLocationDot style={{ color: "#1890ff" }} title="Địa chỉ" />;
}

export default IconLocation;
