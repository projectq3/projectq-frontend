import { SendOutlined } from "@ant-design/icons";
import React from "react";

function IconIntroduce({ onClick }) {
  return <SendOutlined style={{ color: "blue" }} onClick={onClick} />;
}

export default IconIntroduce;
