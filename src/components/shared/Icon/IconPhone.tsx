import { PhoneOutlined } from "@ant-design/icons";
import React from "react";

function IconPhone(props) {
  return <PhoneOutlined style={{ color: "#1890ff" }} title="Số điện thoại" />;
}

export default IconPhone;
