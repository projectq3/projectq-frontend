import { CheckOutlined } from "@ant-design/icons";
import React from "react";

function IconCheck(props) {
  return <CheckOutlined style={{ color: "#1890ff" }} title="Thanh toán" />;
}

export default IconCheck;
