import { Avatar, Card, Button, Divider } from "antd";
import {
  EditOutlined,
  EllipsisOutlined,
  FacebookOutlined,
  SettingOutlined,
  YoutubeOutlined,
} from "@ant-design/icons";
import Meta from "antd/lib/card/Meta";
import User from "../../../assets/images/bg/User.png";
import toi from "../../../assets/images/profile/toi.jpg";
import bg1 from "../../../assets/images/bg/bg1.png";
import bg2 from "../../../assets/images/bg/bg2.jpg";
import bg3 from "../../../assets/images/bg/bg3.jpg";
import bg4 from "../../../assets/images/bg/bg4.jpg";

const CardMyProfile = () => {
  return (
    <div>
      <Card
        style={{ width: "100%" }}
        // cover={<img alt="example" src={User} />}
        actions={[
          <SettingOutlined key="setting" />,
          <EditOutlined key="edit" />,
          <EllipsisOutlined key="ellipsis" />,
          <Button type="primary" shape="round" danger>
            Follow
          </Button>,
        ]}
      >
        <Meta
          avatar={<Avatar src={toi} style={{ width: 100, height: 100 }} />}
          title="Phạm Văn Quỳnh"
          description={
            <div>
              <p>WEB DEV FULLSTACK</p>
              <p>
                <span style={{ fontWeight: "bold" }}>Dob: </span>
                <span>30/07/2002</span>
              </p>
              <p>
                <span style={{ fontWeight: "bold" }}>Gender: </span>
                <span>Male</span>
              </p>
              <p>
                <span style={{ fontWeight: "bold" }}>Phone: </span>
                <span>0379128012</span>
              </p>
              <p>
                <span style={{ fontWeight: "bold" }}>Email: </span>
                <span>phamvanquynhk44@gmail.com</span>
              </p>
              <p>
                <span style={{ fontWeight: "bold" }}>Address: </span>
                <span>Phuoc Long B Ward, Thu Duc City, Ho Chi Minh City </span>
              </p>
              {/* <div>
                <Button
                  type="primary"
                  // danger
                  icon={<FacebookOutlined />}
                >
                  Follow
                </Button>
              </div>
              <div>
                <Button type="primary" danger icon={<YoutubeOutlined />}>
                  Follow
                </Button>
              </div> */}
            </div>
          }
        />
      </Card>
    </div>
  );
};

export default CardMyProfile;
