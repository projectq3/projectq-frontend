import { Card, Checkbox, Col, Form, Skeleton } from "antd";
import { SearchInput } from "src/components";

const CardDisease = ({
  loading,
  loadingDisease,
  setSearchQueryDisease,
  DiseaseCategories,
  selectedDiseases,
  handleSelectedDiseases,
}) => {
  return (
    <div>
      <Card
        title="Chuẩn đoán"
        style={{
          marginBottom: 16,
          height: "unset",
        }}
        loading={loading}
        size="small"
      >
        <Form.Item name="type_disease">
          <div>
            <SearchInput
              onSearch={(value) => {
                setSearchQueryDisease(value);
              }}
              title={"Tìm kiếm chuẩn đoán"}
              width={250}
            />
          </div>
          <Skeleton loading={loadingDisease}>
            <div className="scroll-checkbox">
              {DiseaseCategories.map((item) => (
                <Col key={item.id} span={24} style={{ marginBottom: 5 }}>
                  <Checkbox
                    checked={selectedDiseases.includes(item.id)}
                    onChange={() => handleSelectedDiseases(item.id)}
                  >
                    {`${item.name}`}
                  </Checkbox>
                </Col>
              ))}
            </div>
          </Skeleton>
        </Form.Item>
      </Card>
    </div>
  );
};

export default CardDisease;
