import { Button, Carousel, Image } from "antd";
import bg1 from "../../../assets/images/bg/bg1.png";
import bg2 from "../../../assets/images/bg/bg2.jpg";
import bg3 from "../../../assets/images/bg/bg3.jpg";
import bg4 from "../../../assets/images/bg/bg4.jpg";
import ImageGallery from "react-image-gallery";
import { DownloadOutlined, EyeOutlined } from "@ant-design/icons";
import { useState } from "react";
import { SizeType } from "antd/es/config-provider/SizeContext";

const CarouselComponent = () => {
  // const contentStyle: React.CSSProperties = {
  //   height: "260px",
  //   color: "#fff",
  //   // lineHeight: "160px",
  //   textAlign: "center",
  //   background: "#364d79",
  // };

  // const images = [
  //   {
  //     original: `${bg1}`,
  //     thumbnail: `${bg1}`,
  //   },
  //   {
  //     original: "https://picsum.photos/id/1015/1000/600/",
  //     thumbnail: "https://picsum.photos/id/1015/250/150/",
  //   },
  //   {
  //     original: "https://picsum.photos/id/1019/1000/600/",
  //     thumbnail: "https://picsum.photos/id/1019/250/150/",
  //   },
  // ];
  const [size, setSize] = useState<SizeType>("large"); // default is 'middle'
  return (
    <div>
      {/* <img
        alt="example"
        src={bg4}
        width={1010}
        height={300}
        style={{ borderRadius: "10px" }}
      />
      <div className="bg-profile">
        <Button
          type="primary"
          shape="round"
          danger
          icon={<DownloadOutlined />}
          size={size}
        >
          Tải CV
        </Button>
        <Button
          type="primary"
          shape="round"
          // danger
          icon={<EyeOutlined />}
          size={size}
        >
          Xem CV
        </Button>
      </div> */}
      <div style={{ position: "relative" }}>
        <img
          alt="example"
          src={bg4}
          width={1010}
          height={300}
          style={{ borderRadius: "10px" }}
        />
        <div className="bg-profile">
          <Button
            type="primary"
            shape="round"
            danger
            icon={<DownloadOutlined />}
            size={size}
          >
            Tải CV
          </Button>
        </div>
      </div>
    </div>

    // <Carousel autoplay>
    //   <div>
    //     <h3 style={contentStyle}>
    //       <Image src={bg1} />
    //     </h3>
    //   </div>
    //   <div>
    //     <h3 style={contentStyle}>
    //       <Image src={bg2} />
    //     </h3>
    //   </div>
    //   <div>
    //     <h3 style={contentStyle}>
    //       <Image src={bg3} />
    //     </h3>
    //   </div>
    //   <div>
    //     <h3 style={contentStyle}>
    //       <Image src={bg4} />
    //     </h3>
    //   </div>
    // </Carousel>
    // <ImageGallery items={images} />
  );
};

export default CarouselComponent;
