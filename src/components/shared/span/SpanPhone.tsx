import React from "react";

const SpanPhone = ({ phone }) => {
  return (
    <span>
      <a href={`tel:${phone}`}>{phone}</a>
    </span>
  );
};

export default SpanPhone;
