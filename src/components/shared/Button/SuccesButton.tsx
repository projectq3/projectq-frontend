import React from "react";
import { Button } from "antd";

const SuccesButton = ({ onClick, disabled, title }) => (
  <Button
    onClick={onClick}
    type="primary"
    className="btn-success"
    disabled={disabled}
  >
    {title}
  </Button>
);

export default SuccesButton;
