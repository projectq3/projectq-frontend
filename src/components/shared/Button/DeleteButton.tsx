import React from "react";
import { Button } from "antd";

const DeleteButton = ({ onClick, disabled , title }) => (
  <Button disabled={disabled} onClick={onClick} type="primary" danger>
    {title}
  </Button>
);

export default DeleteButton;
