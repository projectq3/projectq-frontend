import React from 'react';
import { Button } from 'antd';

const EditButton = ({ onClick ,title }) => (
  <Button
    onClick={onClick}
    type="primary"
  >
    {title}
  </Button>
);

export default EditButton;
