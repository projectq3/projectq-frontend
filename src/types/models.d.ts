import {UserProperty} from './types';

export type StoreState = {
  user: UserProperty;
  first: boolean;
  token: string;
  StoreState: number;
  setting: any;
};
