export type StoreState = {
  user: UserProperty; 
  setting: Object;
};

export interface UserProperty {
  id: number;
  name: string;
  email: string | null;
  phone: string;
  avatar: string;
  dob: string | null;
  gender: string | "male" | "female" | "order";
  description: string | null;
  member_type: UserTypeProperty;
  specialist: string | null;
  address: string | null;
  certificate_code: string | null;
  bank_name: string | null;
  bank_number: string | null;
  sign_allergy: string | null;
  hospital_name: string | null;
  hospital_address: string | null;
  accept_policy: any;
  bank_qr_code?: {
    file_url: string;
    url: string;
    uri: string;
    name: string;
  };
}

type PacketTestType = {
  id: number;
  name: string;
  price: number;
  price_formated: string;
};

type CategoriesType = {
  id: number;
  name: string;
  type_tests: TestType[];
};

export interface UserFormDataProperty {
  name: string;
  specialist: string;
  dob: any;
  certificate_code: string;
  hospital_name: string;
  hospital_address: string;
  email: string;
  bank_name: string;
  bank_number: string;
  gender: string;
}

type OrderPatientType = {
  id: number;
  file_pdf: string;
  day_of_week: date;
  day_format: string;
  packet: string;
  total: string;
  type_tests: { id: number; name: string }[];
};

export interface CreateOrderTestFormData {
  member_name: string;
  member_dob: string;
  member_gender: string;
  member_email: string;
  member_address: string;
  member_phone: string;
  patient_id: number;
  packet_test_id: number;
  type_test: number[];
  sampling_package_id: number;
  total: number;
  images: any | null;
}

export interface ServiceClinicProperties {
  id: number;
  name: string;
  price: string | number;
}

interface SignatureProperties {
  id: number;
  image: string;
  name: string;
  active: boolean;
  date: string;
}

interface HeadLetterProperties {
  id: number;
  image: string;
  name: string;
  active: boolean;
  date: string;
}

export interface ClinicSlotFormData {
  service_id: number;
  date: string;
  slots: MedicalExaminationSlot[];
}

export interface MedicalExaminationSlot {
  id?: number | string;
  enable?: boolean;
  start_time: string;
  end_time: string;
  patient_quantity: number;
  end_date?: any;
}