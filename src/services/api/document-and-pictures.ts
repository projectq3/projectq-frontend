import network from "../network/network";

type NetworkPromiseResponse<T> = Promise<T>;

function getAllDocumentAndPictures<T>(
  patientid: number,
  per_page: number,
  page: number
): NetworkPromiseResponse<T> {
  return new Promise((resolve, reject) => {
    network
      .get(`v1/all-file/${patientid}?per_page=${per_page}&page=${page}`)
      .then((res: any) => {
        if (res.status >= 400) {
          reject(res);
        }
        resolve(res);
      })
      .catch((err: any) => {
        reject(err);
      });
  });
}

function getAllDocumentAndPicturesByDate<T>(
  date: string
): NetworkPromiseResponse<T> {
  return new Promise((resolve, reject) => {
    network
      .get(`v1/all-file-by-date/${date}`)
      .then((res: any) => {
        if (res.status >= 400) {
          reject(res);
        }
        resolve(res);
      })
      .catch((err: any) => {
        reject(err);
      });
  });
}

function DocumentAndPicturesAdd<T>(
  formData: FormData
): NetworkPromiseResponse<T> {
  return new Promise((resolve, reject) => {
    network
      .post(`v1/documents-pictures/create`, formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      })
      .then((res: any) => {
        if (res.status >= 400) {
          reject(res);
        }
        resolve(res);
      })
      .catch((err: any) => {
        reject(err);
      });
  });
}

const data = {
  getAllDocumentAndPictures,
  getAllDocumentAndPicturesByDate,
  DocumentAndPicturesAdd,
};

export default data;
