import network from "../network/network";

type NetworkPromiseResponse<T> = Promise<T>;

function storeClinicSlot<T>(
  formData: any
): NetworkPromiseResponse<T> {
  return new Promise((resolve, reject) => {
    network
      .post("v1/clinic-slot/create", formData)
      .then((res: any) => {
        if (res.status >= 400) {
          reject(res);
        }
        resolve(res);
      })
      .catch((err: any) => {
        reject(err);
      });
  });
}

function getClinicSlot<T>(
  serivce_id: number,
  date: string
): NetworkPromiseResponse<T> {
  return new Promise((resolve, reject) => {
    network
      .get(`v1/clinic-slot?date=${date}&service_id=${serivce_id}`)
      .then((res: any) => {
        if (res.status >= 400) {
          reject(res);
        }
        resolve(res);
      })
      .catch((err: any) => {
        reject(err);
      });
  });
}

function updateClinicSlot<T>(
  id: number,
  formData: {
    date: string;
    patient_quantity: number;
    repeat: "daily" | "onlyday";
  }
): NetworkPromiseResponse<T> {
  return new Promise((resolve, reject) => {
    network
      .post(`v1/clinic-slot/edit/${id}`, formData)
      .then((res: any) => {
        if (res.status >= 400) {
          reject(res);
        }
        resolve(res);
      })
      .catch((err: any) => {
        reject(err);
      });
  });
}
function deleteClinicSlot<T>(
  id: number,
  formData: {date: string; repeat: 'daily' | 'onlyday'},
): NetworkPromiseResponse<T> {
  return new Promise((resolve, reject) => {
    network
      .delete(`v1/clinic-slot/${id}`, {data : formData})
      .then((res: any) => {
        if (res.status >= 400) {
          reject(res);
        }
        resolve(res);
      })
      .catch((err: any) => {
        reject(err);
      });
  });
}

const data = {
  getClinicSlot,
  storeClinicSlot,
  updateClinicSlot,
  deleteClinicSlot,
};

export default data;
