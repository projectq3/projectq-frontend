import {
  BellOutlined,
  ExclamationCircleOutlined,
  FlagOutlined,
  InfoCircleOutlined,
  LogoutOutlined,
  QrcodeOutlined,
  SearchOutlined,
  SettingOutlined,
  UserDeleteOutlined,
  UserOutlined,
} from "@ant-design/icons";
import {
  Layout,
  Menu,
  Modal,
  Drawer,
  Tooltip,
  List,
  Form,
  Input,
  message,
  FloatButton,
  Button,
} from "antd";
import { ReactElement, useEffect, useState } from "react";
import { useRouteMatch, useHistory } from "react-router-dom";
import itemsMenu from "../menu/index";
import { api } from "src/services";
import { useDispatch } from "react-redux";
import { allActions } from "src/redux";
// import LogoImed from "../assets/imagesg";
import { images } from "src/assets";
import { utils } from "src/utils";
import { DropdownHeader } from "src/components";
import "react-image-gallery/styles/scss/image-gallery.scss";
import {
  FaBookBookmark,
  FaCapsules,
  FaFileContract,
  FaSignature,
  FaUnlock,
  FaUserXmark,
} from "react-icons/fa6";
import { Footer } from "antd/es/layout/layout";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";

const { Header, Content } = Layout;

interface Props {
  children: ReactElement | ReactElement[];
}

function Admin({ children }: Props) {
  const history = useHistory();
  const [openDrawer, setOpenDrawer] = useState(false);
  const [notifications, setNotifications] = useState([]);
  const [loading, setLoading] = useState(false);
  const [hasUnreadNotification, setHasUnreadNotification] = useState(false);
  const [userLogin, setUserLogin]: any = useState();
  const [userContract, setUserContract]: any = useState({});
  const [isOpenModalDeleteUser, setIsOpenModalDeleteUser] = useState(false);
  const [form] = Form.useForm();
  let { path } = useRouteMatch();
  const dispatch = useDispatch();

  useEffect(() => {
    fetchNotification();
    fetchUserLogin();
  }, []);

  useEffect(() => {
    setHasUnreadNotification(notifications.some((item) => !item?.is_read));
  }, [notifications]);

  const fetchNotification = async () => {
    setLoading(true);
    // const { data: rData }: any = await api.getNotifications();
    setLoading(false);
    // if (rData) {
    //   setNotifications([]);
    // }
  };

  const fetchUserLogin = async () => {
    setLoading(true);
    // const { data: rData }: any = await api.getProfile();
    setLoading(false);
    // if (rData && rData.userData) {
    //   setUserLogin(rData.userData);
    //   if (rData.userData.contract) {
    //     setUserContract(rData.userData.contract);
    //   } else {
    //     setUserContract([]);
    //   }
    // }
  };
  const handleReadNotification = async (id) => {
    try {
      setLoading(true);
      const response: any = await api.readNotification(id);
      setLoading(false);
      if (response && !response.error) {
        fetchNotification();
      }
    } catch (err) {
      message.error("Đánh dấu thông báo đã xem thất bại");
      setLoading(false);
    }
  };

  const onSelecteMenu = ({ key }: { key: string }) => {
    if (key === "logout") {
      Modal.confirm({
        okText: "Logout",
        cancelText: "Cancel",
        title: "Do you want to logout?",
        icon: <ExclamationCircleOutlined />,
        onOk() {
          return new Promise((resolve, reject) => {
            api
              .logout()
              .then((res) => {
                localStorage.removeItem("access_token");
                dispatch(allActions.user.deleteUser());
                history.push("/auth/login");
                resolve(res);
              })
              .catch((error) => reject(error));
          });
        },
        onCancel() {
          console.log("Cancel delete");
        },
      });
      return;
    }
    history.push(`${path}${key}`);
  };

  const itemUser = [
    {
      label: (
        <span onClick={() => history.push("/personal-information")}>
          <InfoCircleOutlined className="icon-header" />
          Thông tin cá nhân
        </span>
      ),
      // key: "0",
    },
    {
      label: (
        <span onClick={() => history.push("/shareqrcode")}>
          <QrcodeOutlined className="icon-header" />
          Qr Code
        </span>
      ),
      // key: "1",
    },
    {
      label: (
        <span onClick={() => history.push("/termsandconditions")}>
          <FaBookBookmark className="icon-header" />
          Điều khoản & chính sách
        </span>
      ),
      // key: "2",
    },
    userContract.url && {
      label: (
        <span onClick={() => window.open(userContract.url)}>
          <FaFileContract className="icon-header" />
          Thông tin hợp đồng
        </span>
      ),
      // key: "2",
    },
    {
      label: (
        <span onClick={() => history.push("/change-password")}>
          <FaUnlock className="icon-header" />
          Đổi mật khẩu
        </span>
      ),
      // key: "3",
    },
    {
      label: (
        <span onClick={() => onSelecteMenu({ key: "logout" })}>
          <LogoutOutlined className="icon-header" />
          Đăng xuất
        </span>
      ),
      // key: "4",
    },
    {
      label: (
        <span
          onClick={() => showConfirmDeleteAccount()}
          style={{ color: "red" }}
        >
          <FaUserXmark className="icon-header" />
          Xóa tài khoản
        </span>
      ),
      // key: "5",
    },
  ];

  const itemSetting = [
    {
      label: (
        <span onClick={() => history.push("/dr/signature")}>
          <FaSignature className="icon-header" />
          Signature Phòng khám
        </span>
      ),
      key: "0",
    },
    {
      label: (
        <span onClick={() => history.push("/dr/headLetter")}>
          <FlagOutlined className="icon-header" />
          Head Letter phòng khám
        </span>
      ),
      key: "1",
    },
    {
      label: (
        <span onClick={() => history.push("/medicine")}>
          <FaCapsules className="icon-header" />
          Danh mục thuốc
        </span>
      ),
      key: "2",
    },
  ];

  const showConfirmDeleteAccount = () => {
    Modal.confirm({
      okText: "Yes",
      cancelText: "Cancel",
      title: "Do you want to delete this account?",
      icon: <UserDeleteOutlined style={{ color: "red" }} />,
      onOk() {
        setIsOpenModalDeleteUser(true);
      },
      onCancel() {
        console.log("Cancel delete");
      },
    });
    return;
  };

  const handleCancel = () => {
    setIsOpenModalDeleteUser(false);
    form.resetFields();
  };

  const handleDeleteAccount = async (fieldValues: any) => {
    try {
      setIsOpenModalDeleteUser(false);
      const response: any = await api.deleteAccount(fieldValues.phone);
      if (!response?.error) {
        message.success(response.message);
        history.push("/auth/login");
      } else {
        message.error(response.message);
      }
    } catch (error) {
      message.error(error.message);
      setLoading(false);
    }
  };

  return (
    <Layout className="admin">
      <Layout className="site-layout">
        <Header style={{ display: "flex", alignItems: "center" }}>
          <div className="logo">
            <a href="/">
              <img src={images.header.user}></img>
            </a>
          </div>
          <Menu
            onClick={onSelecteMenu}
            theme="dark"
            mode="horizontal"
            selectedKeys={[history.location.pathname.replace(path, "")]}
            items={itemsMenu}
            style={{
              display: "flex",
              justifyContent: "center",
              width: "30%",
              background: "#0F1416",
            }}
          />
          <div
            className="menu-item"
            style={{
              position: "absolute",
              right: "10rem",
              paddingBottom: "1rem",
            }}
          >
            <ul className="top-menu">
              <li className="top-menu-item">
                <DropdownHeader
                  itemsProps={itemUser}
                  HTMLProps={
                    <div className="avatar-container">
                      <img
                        className="avatar"
                        src={images?.header?.user}
                        alt="User Avatar"
                      />
                      <span style={{ color: "#fff" }}>
                        <h3>{"Phạm văn Quỳnh"}</h3>
                      </span>
                    </div>
                  }
                />
              </li>
              <li className="top-menu-item">
                <div className="form-search">
                  <Input
                    size="large"
                    placeholder="Tìm kiếm"
                    prefix={<SearchOutlined />}
                  />
                </div>
              </li>
              <li className="top-menu-item">
                <Tooltip title="Thông báo">
                  <div
                    className="avatar-container"
                    style={{ color: "#fff", position: "relative" }}
                  >
                    <BellOutlined onClick={() => setOpenDrawer(true)} />
                    {hasUnreadNotification && (
                      <span
                        className="dot"
                        style={{
                          position: "absolute",
                          top: -4,
                          right: -4,
                        }}
                      ></span>
                    )}
                  </div>
                </Tooltip>
              </li>
            </ul>
            <Drawer
              title="Thông báo"
              width={500}
              onClose={() => setOpenDrawer(false)}
              open={openDrawer}
            >
              <List
                dataSource={notifications}
                loading={loading}
                renderItem={(item: any) => (
                  <List.Item style={{ width: "100%" }}>
                    {item?.is_read ? (
                      <div className="notification-group">
                        <div className="notification-item">
                          <div style={{ fontWeight: "500" }}>
                            {/* {utils.formatDate(item?.created_at)} */}
                          </div>
                          <div className="notification-item-content">
                            {item?.data?.message}
                          </div>
                        </div>
                      </div>
                    ) : (
                      <div
                        className="notification-group"
                        style={{ cursor: "pointer" }}
                        onClick={() => handleReadNotification(item?.id)}
                      >
                        <div
                          className="notification-item"
                          style={{ fontWeight: "700", flexBasis: "90%" }}
                        >
                          {/* <div>{utils.formatDate(item?.created_at)}</div> */}
                          <div className="notification-item-content">
                            {item?.data?.message}
                          </div>
                        </div>
                        <span className="dot"></span>
                      </div>
                    )}
                  </List.Item>
                )}
              />
            </Drawer>
          </div>
        </Header>
        <Content>
          <div className="container_primary">{children}</div>
        </Content>
        <Footer style={{ textAlign: "center" }}>
          Ant Design ©2023 Created by Ant UED
        </Footer>
      </Layout>

      <Modal
        title="Xóa tài khoản"
        open={isOpenModalDeleteUser}
        onOk={form.submit}
        onCancel={handleCancel}
      >
        <Form form={form} onFinish={handleDeleteAccount} layout="vertical">
          <Form.Item
            label="Vui lòng nhập số điện thoại đã đăng ký để xác nhận thao tác"
            name="phone"
            rules={[
              {
                required: true,
                message: "Số điện thoại không được để trống",
              },
              {
                min: 10,
                max: 11,
                message: "Số điện thoại chưa chính xác",
              },
            ]}
          >
            <Input type="number" placeholder="Nhập số điện thoại" />
          </Form.Item>
        </Form>
      </Modal>
    </Layout>
  );
}

export default Admin;
